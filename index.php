<?php

declare( strict_types= 1);


include_once 'mathFunctions/distanceFunctions.php';
include_once 'mathFunctions/arrayFunctions.php';
include_once 'mathFunctions/myFunctions.php';



use mathFunctions\arrayFunctions as arrayFunc;
use mathFunctions\myFunctions as myFunc;

/**
 * Задача1:
 * №1
 * округлите число, записанное в переменную $number, до двух знаков после запятой и выведите результат на экран;
 */

function rounding(float $number): float {
    echo round($number, 2, PHP_ROUND_HALF_DOWN);
};

rounding(15.655565655);


/**
 * Задача1:
 * №2
 * дана строка 'php', сделайте из нее строку 'PHP';
 */

function uppercase(string $str): sting {
    echo strtoupper($str);
};

uppercase('php');


/**
 * Задача1:
 * №3
 * дана строка 'PHP', сделайте из нее строку 'php';
 */

function normaltext(string $string): sting {
    echo strtolower($string);
};

normaltext('PHP');


/**
 * Задача1:
 * №4
 * напишите функцию, которая принимает строку с названиями 4 овощей для салата через комму,
 * а возвращает массивы из 4 элементов, вызовите функцию и поместите все 4 элемента в отдельную переменную в
 * одно действие;
 */

function arrayProduct(string $product): array {
    return explode(',',$product);
};

$product  = arrayProduct('p1,p2,p3,p4');


/**
 * Задача1:
 * №5
 * дана строка '31.12.2013', замените все точки на дефисы;
 */

function formatDate(string $date): string {
    echo str_replace('.', '-', $date);
};

formatDate('31.12.2013');



/**
 * Задача1:
 * №6
 * есть переменная $password, в которой хранится пароль пользователя, напишите функцию,
 * которая выведите пользователю сообщение о том, что если количество символов пароля больше 5-ти и меньше 10-ти,
 * то пароль подходит, иначе сообщение о том, что нужно придумать другой пароль;
 */

function userPassword(string $password): int {
    $passwordLength = strlen($password);
    if($passwordLength > 5 && $passwordLength < 10) {
        echo 'пароль подходит';
    }else{
        echo 'нужно придумать другой пароль';
    };
};

userPassword("sdjfsjfs2");


/**
 * Задача1:
 * №7
 * дана строка '1234567890', разбейте ее на массив с элементами по 2 цифры в каждом;
 */

function arrayNumber(string $string): array {
    return str_split($string, 2);
};

print_r(arrayNumber('1234567890'));


/**
 * Задача1:
 * №8
 * сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на русском языке.
 */

function days(int $numberDay): string {
    $day =['понедельник','вторник','среда','четверг','пятница','суббота','воскресенье'];
    return $day[$numberDay-1];
}

echo days(7);


/**
 * Задача1:
 * №9
 * дан массив 'a'=>1, 'b'=>2, 'c'=>3, поменяйте в нем местами ключи и значения;
 */

function replacement(): array {
    $number = ['a'=>1, 'b'=>2, 'c'=>3];
    $result = array_flip ($number);
}

replacement();


/**
 * Задача1:
 * №10
 * найдите все счастливые билеты, счастливый билет - это билет, в котором сумма первых трех
 * цифр его номера равна сумме вторых трех цифр его номера, функция должна принимать номер
 * билета и возвращать true или false;
 */

function happyTicket($ticket): bool {
    $arrayTicket = str_split((string) $ticket);
    $result = false;

    $sum1 = array_slice($arrayTicket, 0, 3);
    $sum2 = array_slice($arrayTicket, 3);

    if(array_sum($sum1) === array_sum($sum2)){
        $result = true;
    }else{
        $result = false;
    }
    return $result;
};

echo happyTicket(555555);


/**
 * Задача1:
 * №11
 * сделайте функцию, которая отнимает от первого числа второе и делит на третье;
 */

function sum(int $number1, int $number2, int $number3): int {
    echo (($number1 - $number2) / $number3);
};

sum(20, 10, 5);


/**
 * Задача1:
 * №12
 * дан массив с элементами 1, 2, 3, 4, 5, с помощью функции array_slice
 * создайте из него массив $result с элементами 2, 3, 4;
 */

function arrayDivision(array $array): array {
    return array_slice($array, 1, 3);
}

$result = arrayDivision([1, 2, 3, 4, 5]);

/**
 * Задача1:
 * №13
 * есть строка, проверьте через функцию, что она начинается на 'http://' или на 'https://',
 * если это так, выведите 'да', если не так - 'нет';
 */

function validationLink(string $link): sting {
    $protocol = ['http://', 'https://'];
    if(strpos($link, $protocol[0]) === 0 || strpos($link, $protocol[1]) === 0){
        echo 'да';
    }else{
        echo 'нет';
    }
}

validationLink('https://itea.local/');


/**
 * Задача1:
 * №14
 * дана строка $str, замените в ней все буквы 'a' на цифру 1, буквы 'b' - на 2, а буквы 'c' - на 3,
 * решите задачу двумя способами работы с функцией strtr (массив замен и две строки замен);
 */

//способ №1
function replaceString(string $str): string {
    $arrReplace = ["a" => 1, "b" => 2, "c" => 3];
    return strtr ($str , $arrReplace);
}

echo replaceString('adbcl');

//способ №2
function replaceString2(string $str){
    $arrReplace = ["a" => 1, "b" => 2, "c" => 3];
    foreach ($arrReplace as $key => $value){
        $str = strtr($str, (string) $key,(string) $value);
    }

    return  $str;
}

echo replaceString2('adbclasdasdasdascc');



/**
 * Задача1:
 * №15
 * сделайте функцию getFormattedDate, которая принимает на вход три параметра: день, месяц и год рождения,
 * а возвращает их строкой в отформатированном виде, например: 30:02:1953, день и месяц нужно форматировать так,
 * чтобы при необходимости добавлялся 0 слева. (например, если в качестве месяца пришла цифра 7,
 * то в выходной строке она должна быть представлена как 07), подсказка - sprintf;
 */

function getFormattedDate (int $day, int $month, int $year): string {
   echo sprintf("%02d/%02d/%02d", $day, $month, $year);
}
getFormattedDate(5,6,93);



/**
 * Задача1:
 * №16
 * напишите функцию (название придумайте сами),
 * которая принимает два года рождения и возвращает строку с разницей в возрасте в виде
 * The age difference is 11;
 */

function ageDifference ($year1, $year2): string {
    $max = max($year1, $year2);
    $min = min($year1, $year2);
    return 'The age difference is'.($max-$min);
}

echo ageDifference(1990, 2000);




/**
 * Задача1:
 * №17
 * реализуйте функцию, которая округляет возраст так,
 * что половина округляется в нижнюю сторону. То есть если человеку десять с половиной лет,
 * то функция должна вернуть 10. Если ему хотя бы немного больше десяти с половиной, то округление идет в верхнюю сторону
 */

function roundingAge($age): int {
    $result = floor ( $age );
    return (integer) $result;
}

echo roundingAge(16.8);



/**
 * Задача1:
 * №18
 * дан массив чисел 4, -2, 5, 19, -130, 0, 10.
 * Напишите функцию, которая вернет минимальное и максимальное число и запишите
 * результат в 2 переменных за одну манипуляцию;
 */

function min_max(array $arrayNumbers){
    $max = max($arrayNumbers);
    $min = min($arrayNumbers);

};
min_max([4, -2, 5, 19, -130, 0, 10]);



/**
 * Задача2:
 * Один ученый любит составлять карты, и ему часто нужно выводить на экран повторяющиеся символы для визуализации маршрутов.
 * Например, так он иллюстрирует грунтовые дороги между городами: Нижние Варты =-=-=-=- Myr (длина "дороги" ровно такая, как в задаче).
 * А так иллюстрирует магистрали: Киев ======== Чоп (длина "дороги" ровно такая, как в задаче).
 * В документации PHP ученый нашёл функцию str_repeat(), которая возвращает повторяющуюся строку;
 * Использую эту функцию, напишите свою, которая принимает 4 аргумента: 2 строки с названиями городов,
 * количество повторений разделителя и сам разделитель, как не обязательный аргумент (по умолчанию ‘=’), который должен дублироваться;
 * Цель функции - соединить 2 города с помощью дороги (повторяющегося разделителя) в одну строку;
 * Используйте все возможные варианты строгой типизации.

 */


function mapView(string $city1,  string $city2, int $quantity, string $splitter): string {
    $road = str_repeat($splitter, $quantity);
    return $city1." ".$road." ".$city2;
}

echo mapView('Варты', 'Myr', 8, '=-');


echo distance(5, 1, 2,4);
echo distance(10, 15, 8,14);
echo distance(30, 7, 4,2);


echo arrayFunc\eveningItems([10,15,15,15,13]);

echo myFunc\validPassword('sdjfфівфі');
myFunc\clients(['director' => "Victor", 'manager' => 'Igor']);
echo myFunc\oldestEmployee(['Igor' => 20, 'Inna' => 30, 'Vasya' => 35]);
myFunc\sortArray(['Igor' => 20, 'Inna' => 30, 'Vasya' => 35]);
echo myFunc\typeData(25545);
echo myFunc\timeSecond();

myFunc\arrayIsset($arr);
myFunc\arrayEmpty($arr);
myFunc\numbersAll(30);